LOCAL_PATH := vendor/gapps/system

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/lib/libjni_latinimegoogle.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libjni_latinimegoogle.so \
    $(LOCAL_PATH)/lib64/libjni_latinimegoogle.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libjni_latinimegoogle.so \
    $(LOCAL_PATH)/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.dialer.support.xml \
    $(LOCAL_PATH)/etc/permissions/com.google.android.maps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.maps.xml \
    $(LOCAL_PATH)/etc/permissions/privapp-permissions-google-p.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-google-p.xml \
    $(LOCAL_PATH)/etc/permissions/privapp-permissions-google-ps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-google-ps.xml \
    $(LOCAL_PATH)/etc/permissions/privapp-permissions-google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-google.xml \
    $(LOCAL_PATH)/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    $(LOCAL_PATH)/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google.xml \
    $(LOCAL_PATH)/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_build.xml
    $(LOCAL_PATH)/app/MarkupGoogle/lib/arm64/libsketchology_native.so:$(TARGET_COPY_OUT_SYSTEM)/app/MarkupGoogle/lib/arm64/libsketchology_native.so