$(call inherit-product, vendor/gapps/common-blobs.mk)

PRODUCT_SOONG_NAMESPACES += \
    vendor/gapps/system

PRODUCT_PACKAGES += \
    MarkupGoogle \
    SetupWizardPrebuilt \
    AndroidMigratePrebuilt \
    PrebuiltGmsCore \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    PrebuiltExchange3Google \
    GoogleFeedback \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    ConfigUpdater \
    GoogleRestore \
    com.google.android.dialer.support \
    com.google.android.maps